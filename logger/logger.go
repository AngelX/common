package logger

import (
	"context"
	"github.com/sirupsen/logrus"
	"go.elastic.co/ecslogrus"
	"os"
)

type Config struct {
	// FilePath is a path to output file. if empty will use os.Stdout
	FilePath string
	// Env is  determinate what environment is used
	Env string
}

type Logger struct {
	logEngine *logrus.Logger
}

// New is an implementation of ElasticSearch designed log format
// with capability to stack messages or Elastic Fields(properties)
func New(c *Config) *Logger {
	logger := logrus.New()

	logger.SetLevel(logrus.TraceLevel)
	logger.SetFormatter(&ecslogrus.Formatter{})
	logger.SetOutput(os.Stdout)
	logger = logger.WithField("Env", c.Env).Logger

	if c.FilePath != "" {
		f, err := os.OpenFile(c.FilePath, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
		if err != nil {
			logger.Print("error opening file:")
			logger.Panic(err.Error())
			return nil
		} else {
			logger.SetOutput(f)
		}
	}
	l := &Logger{
		logEngine: logger,
	}
	return l
}

// NewCollector returns Entity of struct is an object to stack logs
// should use Commit() as a defer to actually print out all collected logs
func (p *Logger) NewCollector() *Entity {
	ctx := context.Background()
	return &Entity{
		fields:   make(map[string]interface{}),
		logger:   p.logEngine.WithContext(ctx).Logger,
		logTrace: []string{},
	}
}
