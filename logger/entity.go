package logger

import "github.com/sirupsen/logrus"

type Entity struct {
	logger   *logrus.Logger
	fields   logrus.Fields
	logTrace []string
}

// SetField adds or replace field in a collection to log
func (p *Entity) SetField(key string, value interface{}) {
	p.fields[key] = value
}

// AddLog add msg to the end of trace log
func (p *Entity) AddLog(msg string) {
	if len(msg) > 0 {
		p.logTrace = append(p.logTrace, msg, "\n")
	}
}

// Commit is a actual print out method that will print all collected messages and fields to output
func (p *Entity) Commit() {
	res := ""
	for _, v := range p.logTrace {
		res += v
	}
	p.logger.WithFields(p.fields).Trace(res)
}
