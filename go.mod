module gitlab.com/AngelX/common

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/joho/godotenv v1.4.0
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.20.2 // indirect
	github.com/rabbitmq/amqp091-go v1.4.0
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.7.0
	go.elastic.co/ecslogrus v1.0.0
	gopkg.in/ini.v1 v1.66.2
)
