package queue

import (
	"context"
	"encoding/json"
	"fmt"
	redisClient "github.com/go-redis/redis"
	"strconv"
)

type redis struct {
	client   *redisClient.Client
	host     string
	port     string
	password string
	db       string
}

var redisSubscriber *redisClient.PubSub

func (c *redis) getConnectionInstance() *redisClient.Client {
	client := c.establishConnection(c.host, c.port, c.password, c.db)

	return client
}

func (c *redis) getSubscriber(channel string) *redisClient.PubSub {
	client := c.getConnectionInstance()

	if redisSubscriber == nil {

		redisSubscriber = client.Subscribe(channel)
	}

	return redisSubscriber
}

func (c *redis) establishConnection(host string, port string, password string, db string) *redisClient.Client {
	intDb, _ := strconv.Atoi(db)

	if c.client != nil {
		return c.client
	}

	client := redisClient.NewClient(&redisClient.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: password,
		DB:       intDb,
	})

	c.client = client

	return client
}

func (c *redis) Publish(ctx context.Context, topic string, message *Message) error {
	data, _ := json.Marshal(message.Data)
	client := c.getConnectionInstance()
	err := client.Publish(topic, data).Err()
	if err != nil {

		return err
	}

	return nil
}

func (c *redis) ReceiveWithDeadLetterDelay(ctx context.Context, topic string, queue string, deadLaterTTL int32, callback func(message []byte) bool) error {
	return c.Receive(ctx, topic, queue, callback)
}

func (c *redis) Receive(ctx context.Context, topic string, queue string, callback func(message []byte) bool) error {
	subscriber := c.getSubscriber(topic)

	msg, err := subscriber.ReceiveMessage()
	if err != nil {

		return err
	}

	requeue := callback([]byte(msg.Payload))

	if requeue {

		c.client.Publish(topic, msg)
	}

	return nil
}

func NewRedisClient(host string, port string, password string, db string) *redis {
	return &redis{
		host:     host,
		port:     port,
		password: password,
		db:       db,
	}
}
