package queue

import (
	"context"
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"strconv"
)

type rabbitmq struct {
	client       *amqp.Connection
	Host         string
	Port         string
	Username     string
	Password     string
	VHost        string
	ExchangeType string
	Args         amqp.Table
}

var rabbitSubscriber *amqp.Channel

func (c *rabbitmq) getChannelInstance() (*amqp.Channel, error) {
	if rabbitSubscriber == nil || rabbitSubscriber.IsClosed() {
		err := c.establishChannel()

		if err != nil {
			return nil, err
		}
	}

	return rabbitSubscriber, nil
}

func (c *rabbitmq) establishChannel() error {
	client, cErr := c.getConnection()

	if cErr != nil {

		return cErr
	}

	ch, err := client.Channel()

	if err != nil {

		return err
	}

	rabbitSubscriber = ch

	return nil
}

func (c *rabbitmq) getConnection() (*amqp.Connection, error) {
	if c.client == nil || c.client.IsClosed() {
		client, err := c.establishConnection(c.Host, c.Port, c.Username, c.Password, c.VHost)

		if err != nil {

			return nil, err
		}

		c.client = client
	}

	return c.client, nil
}

func (c *rabbitmq) establishConnection(host string, port string, username string, password string, vhost string) (*amqp.Connection, error) {
	intPort, _ := strconv.Atoi(port)

	conf := amqp.URI{
		Scheme:   "amqp",
		Host:     host,
		Port:     intPort,
		Username: username,
		Password: password,
		Vhost:    vhost,
	}.String()

	conn, err := amqp.Dial(conf)

	if err != nil {

		return nil, err
	}

	return conn, nil
}

func (c *rabbitmq) Publish(ctx context.Context, topic string, message *Message) error {
	data, _ := json.Marshal(message.Data)
	publisher, err := c.getChannelInstance()

	if err != nil {
		return err
	}

	dErr := publisher.ExchangeDeclare(topic, c.ExchangeType, true, false, false, false, nil)

	if dErr != nil {

		return dErr
	}

	pErr := publisher.PublishWithContext(ctx, topic, "", false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "text/plain",
		Body:         data,
	})

	if pErr != nil {

		return pErr
	}

	return nil
}

// ReceiveWithDeadLetterDelay process incoming message with callback provided.
//
//	callback should return bool which is a "requeue" param.
//	It determines if current message should be requeue or not.
//	FALSE means that message will not be requeued and will be marked as Acknowledged,
//	witch will remove message from future processing.
//
// All processing error should be managed in callback function (logging, errors handling, etc.)
//
// return error in case of error with connection or channel itself,
// so reconnection should be implemented on client side if required.
//
// Example of reconnection in case of failure
//
//	 for {
//		  err := client.Receive(ctx, channel, queue, callback)
//		  if err != nil {
//			  time.Sleep(15 * time.Second)
//		  }
//	 }
func (c *rabbitmq) ReceiveWithDeadLetterDelay(ctx context.Context, topic string, queue string, deadLaterTTL int32, callback func(message []byte) bool) error {
	subscriber, err := c.getChannelInstance()

	if err != nil {
		return err
	}

	q, qdErr := subscriber.QueueDeclare(queue, true, false, false, false, c.Args)

	if qdErr != nil {
		return qdErr
	}

	qbErr := subscriber.QueueBind(q.Name, "", topic, false, nil)

	if qbErr != nil {
		return qbErr
	}

	deadLetterTopic := queue + "_dead_letter_exchange"
	dErr := subscriber.ExchangeDeclare(deadLetterTopic, c.ExchangeType, true, false, false, false, nil)
	if dErr != nil {

		return dErr
	}

	dArgs := amqp.Table{}
	for k, v := range c.Args {
		dArgs[k] = v
	}
	dArgs["x-dead-letter-exchange"] = ""
	dArgs["x-dead-letter-routing-key"] = q.Name
	dArgs["x-message-ttl"] = deadLaterTTL
	dQueue, qdErr := subscriber.QueueDeclare(queue+"_dead_letter_queue", true, false, false, false, dArgs)
	if qdErr != nil {
		return qdErr
	}

	qbErr = subscriber.QueueBind(dQueue.Name, "", deadLetterTopic, false, nil)
	if qbErr != nil {
		return qbErr
	}

	msgs, cErr := subscriber.Consume(q.Name, "", false, false, false, false, nil)

	if cErr != nil {
		return cErr
	}

	for d := range msgs {
		requeue := callback(d.Body)

		if requeue {
			nErr := subscriber.PublishWithContext(ctx, deadLetterTopic, "", false, false, amqp.Publishing{
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         d.Body,
			})
			if nErr != nil {
				return nErr
			}
		}
		aErr := d.Ack(true)

		if aErr != nil {

			return aErr
		}
	}

	return nil
}

// Receive process incoming message with callback provided.
//
//	callback should return bool which is a "requeue" param.
//	It will determinate if current message should be requeue or not.
//	FALSE means that message will not be requiued and will be marked as Acknowledged,
//	witch will remove message from future processing.
//
// All processing error should be managed in callback function (logging, errors handling, etc.)
//
// return error in case of error with connection or channel itself,
// so reconnection should be implemented on client side if required.
//
// Example of reconnection in case of failure
//
//	 for {
//		  err := client.Receive(ctx, channel, queue, callback)
//		  if err != nil {
//			  time.Sleep(15 * time.Second)
//		  }
//	 }
func (c *rabbitmq) Receive(ctx context.Context, topic string, queue string, callback func(message []byte) bool) error {
	subscriber, err := c.getChannelInstance()

	if err != nil {
		return err
	}

	q, qdErr := subscriber.QueueDeclare(queue, true, false, false, false, c.Args)

	if qdErr != nil {
		return qdErr
	}

	qbErr := subscriber.QueueBind(q.Name, "", topic, false, nil)

	if qbErr != nil {
		return qbErr
	}

	msgs, cErr := subscriber.Consume(q.Name, "", false, false, false, false, nil)

	if cErr != nil {
		return cErr
	}

	for d := range msgs {
		requeue := callback(d.Body)

		if requeue {
			nErr := d.Nack(true, true)

			if nErr != nil {
				return nErr
			}

			continue
		}
		aErr := d.Ack(true)

		if aErr != nil {

			return aErr
		}
	}

	return nil
}

func NewRabbitMqClient(host string, port string, username string, password string, exchangeType string, vhost string, queueType string) *rabbitmq {
	args := amqp.Table{"x-queue-type": queueType}

	return &rabbitmq{
		Host:         host,
		Port:         port,
		Username:     username,
		Password:     password,
		VHost:        vhost,
		ExchangeType: exchangeType,
		Args:         args,
	}
}
