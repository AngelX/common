package queue

import (
	"context"
	"time"
)

type Message struct {
	Data     interface{}
	SendTime time.Time
	Name     string
}

type IMessageBroker interface {
	Publish(ctx context.Context, topic string, message *Message) error
	Receive(ctx context.Context, topic string, queue string, callback func(message []byte) bool) error
	ReceiveWithDeadLetterDelay(ctx context.Context, topic string, queue string, deadLaterTTL int32, callback func(message []byte) bool) error
}
