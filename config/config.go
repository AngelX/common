package config

import (
	"github.com/joho/godotenv"
	"gopkg.in/ini.v1"
	"os"
	"regexp"
	"strings"
)

var cfg *ini.File

// Init load config files
// configPath - path to config file related to calling path
// envPath - path to env file replacing placeholders in config file
//
// IMPORTANT!
// envPath does not override system defined ENV it only adds variables that are not defined as a system
// if envPath is empty string it will search only on system defined ENV
// return error when cannot load or read files by provided path
func Init(configPath string, envPath string) error {
	//Init config
	var err error
	cfg, err = ini.Load(configPath)
	if err != nil {
		return err
	}
	if envPath != "" {
		err = godotenv.Load(envPath)
		if err != nil {
			return err
		}
	}
	cfg.ValueMapper = func(s string) string {
		if strings.Contains(s, "${") {
			re := regexp.MustCompile("\\${([A-Z_]+)\\|?(.*?)}")
			match := re.FindStringSubmatch(s)
			envName := ""
			defaultVal := ""
			if len(match) > 1 {
				envName = match[1]
			}
			if len(match) > 2 {
				defaultVal = match[2]
			}
			if str, found := os.LookupEnv(envName); found == true {
				return str
			}
			return defaultVal
		}
		return s
	}
	return nil
}

func Get(section string, key string) string {
	return cfg.Section(section).Key(key).String()
}

func GetInt(section string, key string) int {
	i, err := cfg.Section(section).Key(key).Int()
	if err != nil {
		return 0
	}
	return i
}

func GetBool(section string, key string) bool {
	i, err := cfg.Section(section).Key(key).Bool()
	if err != nil {
		return false
	}
	return i
}
