package config

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"testing"
)

type configTestSuite struct {
	suite.Suite
}

func (s *configTestSuite) SetupSuite() {
}

func TestConfigSuite(t *testing.T) {
	suite.Run(t, new(configTestSuite))
}

func (s *configTestSuite) Test_Init_configPath_Invalid() {
	err := Init("notExists.ini", "")
	assert.Error(s.T(), err)
}

func (s *configTestSuite) Test_Init_envPath_Invalid() {
	err := Init("config.example.ini", "notExists.env")
	assert.Error(s.T(), err)
}

func (s *configTestSuite) Test_Init() {
	err := Init("config.example.ini", "")
	assert.NoError(s.T(), err)
}

func (s *configTestSuite) Test_Get_const() {
	err := Init("config.example.ini", ".env.example")
	assert.NoError(s.T(), err)

	prop := Get("constants", "prop")
	assert.Equal(s.T(), "constant", prop)
}

func (s *configTestSuite) Test_Get_prop_defined() {
	err := Init("config.example.ini", ".env.example")
	assert.NoError(s.T(), err)

	prop := Get("env", "prop_defined")
	assert.Equal(s.T(), "env prop is defined", prop)
}

func (s *configTestSuite) Test_Get_prop_with_default() {
	err := Init("config.example.ini", ".env.example")
	assert.NoError(s.T(), err)

	prop := Get("env", "prop_with_default")
	assert.Equal(s.T(), "fallback_default_value", prop)
}

func (s *configTestSuite) Test_Get_prop_with_env_override() {
	err := Init("config.example.ini", ".env.example")
	assert.NoError(s.T(), err)

	prop := Get("env", "prop_with_env_override")
	assert.Equal(s.T(), "env prop is defined", prop)
}

func (s *configTestSuite) Test_Get_prop_not_defined() {
	err := Init("config.example.ini", ".env.example")
	assert.NoError(s.T(), err)

	prop := Get("env", "prop_not_defined")
	assert.Equal(s.T(), "", prop)
}
